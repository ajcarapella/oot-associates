<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title(''); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo home_url('/'); ?>wp-content/themes/oot/images/favicon-16x16.png">
	<link rel="manifest" href="../images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo home_url('/'); ?>wp-content/themes/oot/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<meta name="google-site-verification" content="q9Q-LVf790I10xozb9kJxWKwr3qcVnsFnkRX6RCkrq0" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-88357499-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<nav class="navbar navbar-default navbar-static-top">
<div class="container-fluid no-pad top-nav-container">
	<div class="container main-width no-pad">
		<div class="row">
			 <div class="col-xs-12 col-sm-7 no-pad">
			 	<a href="<?php echo home_url('/'); ?>"><img class="site-logo" src="<?php echo home_url('/'); ?>/wp-content/themes/oot/images/oot-logo.png" alt="Oot & Associates"/></a>
			 	
			 	<span class="sub-headline">Lawyers for injured workers.</span>
			 </div>
			 
			 <div class="col-xs-12 col-sm-5 no-pad hidden-xs">
			 	<div style="float:right;">
				 	<span class="sub-headline">Contact us for a <u>FREE</u> consultation</span><br>
				 	<h3 class="sh-phone"><a href="tel:800.435.8457">800.435.8457</a></h3>
				 	
				 	<a href="https://www.youtube.com/channel/UCZrhUhoZ0bovjeGGejW9HuA" target="_blank">
					 	<img class="header-social" src="<?php echo home_url('/'); ?>/wp-content/themes/oot/images/header-icon-youtube.png" alt="Youtube" style="margin-left:5px;"/>
				 	</a>
				 	<a href="https://www.facebook.com/Oot-Associates-PLLC-Attorneys-at-Law-219495784797676" target="_blank">
				 		<img class="header-social" src="<?php echo home_url('/'); ?>/wp-content/themes/oot/images/header-icon-facebook.png" alt="Facebook"/>
				 	</a>
			 	</div>
			 </div>
		</div>
	</div>
</div>
  <div class="container-fluid no-pad bottom-nav-container">
    <div class="navbar-header">
	    <div class="mob-phone hidden-sm hidden-md hidden-lg show-tablet-portrait"><a href="tel:8004358457"><i class="fa fa-phone" aria-hidden="true"></i>
 800.435.8457</a></div>
        <a href="https://www.facebook.com/Oot-Associates-PLLC-Attorneys-at-Law-219495784797676" target="_blank"><i id="social-fb" class="fa fa-facebook social-mobile hidden-sm hidden-md hidden-lg show-tablet-portrait" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/channel/UCZrhUhoZ0bovjeGGejW9HuA" target="_blank"><i id="social-yt" class="fa fa-youtube social-mobile hidden-sm hidden-md hidden-lg show-tablet-portrait" aria-hidden="true"></i></a>
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar top-bar"></span>
	        <span class="icon-bar middle-bar"></span>
	        <span class="icon-bar bottom-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar">
             <?php //get_template_part('includes/navbar-search'); ?>
       <?php
            wp_nav_menu( array(
                'theme_location'    => 'navbar-left',
                'depth'             => 2,
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
        
     

     </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>
<div class="overlay-nav"></div>