<footer class="site-footer">
  <div class="row">
    <div class="col-lg-12 site-sub-footer sub-main-width">
      <em>Please be advised that the results achieved in any given case depend upon the exact facts and circumstances of that case. Oot & Associates, PLLC cannot guarantee a specific result in any legal matter. Any testimonial or case result listed on this site is based on an actual legal case and represents the results achieved in that particular case, and does not constitute a guarantee, warranty or prediction of the outcome of any other legal matter. May contain fictional events or scenes, prior results do not guarantee a similar outcome.</em>
      <p>&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>">Oot &amp; Associates, PLLC</a><br>Powered by Total Advertising</p>
    </div>
  </div>
</footer>
<div class="consult-overlay">
</div>
<?php wp_footer(); ?>
</body>
</html>
