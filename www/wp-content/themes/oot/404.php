<?php get_template_part('includes/header'); ?>

<div class="container">
  <div class="row">
    <style>
	    .navbar-default .menu-main-menu-container {
		    background:#36180d;
	    }
	    </style>
    <div class="col-xs-12">
      <div id="content" role="main">
        <div class="alert alert-warning" style="margin-top:150px;margin-bottom:300px;background:#36180d;padding:150px 0;color:#ffffff;text-align: center;">
          <h1 style="color:#ffffff;"><i class="glyphicon glyphicon-warning-sign" style="color:#770010;"></i> <?php _e('Error', 'bst'); ?> 404</h1>
          <p style="color:#ffffff;font-size:18px;"><?php _e('The page you were looking for does not exist.', 'bst'); ?></p>
        </div>
      </div><!-- /#content -->
    </div>
    
   
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
