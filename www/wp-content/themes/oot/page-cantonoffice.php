<?php /*
Template Name: Canton Office Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

			<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
							
							
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">
<h1>Canton Workers' Comp Lawyers</h1>
  <div class="row contact-start">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad">
	  	<h2>We’ve represented over <strong>40,000 workers’ compensation claims!</strong></h2>
	  	<p>In the state of New York injured workers face a system where insurance companies routinely delay payment of income benefits, deny legitimate claims, refuse to approve necessary medical treatment, and refuse to offer settlements to those who do not have legal representation. 
		<br><br>
		If you've been injured on the job or denied by the state, don't hesitate to call our Canton Workers' Comp Lawyers at Oot & Associates. With a staff of well-trained individuals and highly skilled attorneys you have a team of people to help you with your individual needs.</p>
				
		<h2 style="padding:15px 0;">Get Your <u>FREE</u> Consultation Now.</h2>
			  	<p>When you are in need of legal advice with your workers' compensation cases. Call us at 800-435-8457 for a FREE telephone consultation, or in the immediate area at 315-379-1466 to discuss your legal concerns
		<br><br>
		<u>Workers’ Comp Benefits may include:</u><br>
		• Medical treatment from physicians, chiropractors, physical therapists and more<br>
		• Reimbursed for traveling to physicians or therapists, and out-of-pocket expenses related to your work injury<br>
		• Payments equal to 2/3 of your average weekly wage for total disability<br>
		• May be entitled to reduced earning compensation to make up for a decreased ability to work</p>


		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h2>Canton Office</h2><br>
				<img style="width:100%;" src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/canton-office.jpg" alt="Canton Office"/>
			</div>
			<div class="col-xs-12 col-sm-6">
				<h3><br><br>Located on Main Street in Canton, NY. Serving Ogdensburg, Massena, Potsdam, and Watertown.</h3>
				<h4>63 Main Street<br>
				Canton, New York<br>
				13617</h4>
				<h4 class="red">Free Parking</h4>
				<h4>Tel: 315.379.1466<br>
					Fax: 315.379.1433</h4>
			</div>
		</div>
		
		
		
		
<h2 style="clear:both;padding-top:35px;">Office Hours:</h2>
		<h3>Monday -<br>
		Thursday&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9:00am - 4:30pm
		<br><br>
		Friday&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9:00am - 12:00pm
		<br><br>
		Saturday&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Closed
		<br><br>
		Sunday&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Closed
		</h3>
			  
			  
			<br><br>  
			  
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2841.0598995499927!2d-75.17279328447138!3d44.595800279100345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cccf6630de0a9d9%3A0xb3b5b62ece22e9b4!2s63+Main+St%2C+Canton%2C+NY+13617!5e0!3m2!1sen!2sus!4v1487740754632" height="250" frameborder="0" style="border:0; width:100%;" allowfullscreen></iframe>
	  <br><br>
	  </div> 
	  
	  
	  
	  	  
  </div><!-- /.row -->
    
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>