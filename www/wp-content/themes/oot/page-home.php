<?php /*
Template Name: Home Page
*/ 
get_template_part('includes/header'); ?>



<div class="container main-width no-pad pr">
	<div id="homeCarousel" class="carousel slide carousel-fade">
	    <div class="carousel-inner">
	        <div class="item active">
	            <img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/home1.jpg" alt="Slider 1"/>
	            <div class="carousel-caption">
	            	<h2>We understand <br>workplace injuries.</h2>
	            	<h3>Over <strong>40,000</strong> workers compensation <br>cases handled by our legal team.</h3>
	            </div>
	        </div>
	        <div class="item">
	            <img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/home2.jpg" alt="Slider 2"/>
	            <div class="carousel-caption">
	            	<h2>Personal injury lawyers <br>who protect your rights.</h2>
	            	<h3>You focus on getting better. <br>We’ll focus on winning your case.</h3>
	            </div>
	        </div>
	        <div class="item">
	            <img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/home3.jpg" alt="Slider 3"/>
	            <div class="carousel-caption">
	            	<h2>We work to get you the <br>social security disability <br>benefits you deserve.</h2>
	            </div>
	        </div>
	        
		    <ol class="carousel-indicators">
		        <li data-target="#homeCarousel" data-slide-to="0" class="active" contenteditable="false"></li>
		        <li data-target="#homeCarousel" data-slide-to="1" class="" contenteditable="false"></li>
		        <li data-target="#homeCarousel" data-slide-to="2" class="" contenteditable="false"></li>
		    </ol>
	    </div>    
	</div>
	
	
	<div class="container consultation-form">
		<div id="title-bar">
			Schedule Your <br>FREE Consultation
		</div>
		<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
		<div id="thank-you-message">
			<h2>Thank you!</h2>
			<p>Your email has been sent.</p>
			<p>An associate from Oot &amp; Associates will be in touch shortly.</p>
		</div>

		<div class="close-consult">X</div>
	</div>
	
	<div class="consult-activate">
		<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
	</div>
	
	
</div>



<div class="container main-width no-pad case-closed-home hidden-xs">
					<div class="row" style="max-width:1050px;margin:auto;padding:0;">
							<div class="col-xs-12 col-sm-4 cch-block">
								<h3 class="brt">Case Closed!<br>
								Oot Client Awarded<br>
								<strong>$612,000</strong><br>
								Tree Trimmer settles injury claim to back, neck and face.</h3>
							</div>
							<div class="col-xs-12 col-sm-4 cch-block">
								<h3 class="brt">Case Closed!<br>
								Oot Client Awarded<br>
								<strong>$308,000</strong><br>
								College employee settles neck injury case.</h3>
							</div>
							<div class="col-xs-12 col-sm-4 cch-block">
								<h3 class="nbrt">Case Closed!<br>
								Oot Client Awarded<br>
								<strong>$189,000</strong><br>
								Custodian settles neck injury compensation claim.</h3>
							</div>
					</div>
			
					<div class="view-more-cases">
						<a href="<?php echo home_url('/'); ?>case-closed">Click to view more cases ></a>
					</div>
	
</div>
	

	<div class="case-closed-home no-pad container main-width hidden-sm hidden-md hidden-lg" style="padding:15px 0;">
		<div class="row">
			<div id="mobileCaseCarousel" class="carousel slide carousel-fade">
			    <div class="carousel-inner">
			        <div class="item active cch-block">
			            <div class="carousel-caption">
			            	<h2>Case Closed! Oot Client Awarded</h2>
			            	<h3><strong>$612,000</strong>Tree Trimmer settles injury claim to back, neck and face.</h3>
			            </div>
			        </div>
			        <div class="item cch-block">
			            <div class="carousel-caption">
			            	<h2>Case Closed! Oot Client Awarded</h2>
			            	<h3><strong>$308,000</strong>College employee settles neck injury case.</h3>
			            </div>
			        </div>
			        <div class="item cch-block">
			            <div class="carousel-caption">
			            	<h2>Case Closed! Oot Client Awarded</h2>
			            	<h3><strong>$189,000</strong>Custodian settles neck injury compensation claim.</h3>
			            </div>
			        </div>
			    </div>    
			</div>
		</div>
		
	</div>
	

</div>


<div class="container sub-main-width no-pad type-claims">
	<h2>Over 100 years of combined experience in representing workers’ compensation, social security disability and personal injury/negligence claims.</h2>
</div>
<div class="container sub-main-width no-pad circle-points no-pad">
	<div class="row no-pad">
		<div class="homepage-service-button col-xs-4 no-pad">
			<a href="<?php echo home_url('/'); ?>practice-areas/workers-compensation">
				<div class="homepage-service-circle"><div id="homepage-service-circle-work"></div></div><br>
				Workplace <br>Injury
			</a>
		</div>
		<div class="homepage-service-button col-xs-4 no-pad">
			<a href="<?php echo home_url('/'); ?>practice-areas/social-security-disability">
				<div class="homepage-service-circle"><div id="homepage-service-circle-ss"></div></div><br>
				Social Security <br>Disability
			</a>
		</div>
		<div class="homepage-service-button col-xs-4 no-pad">
			<a href="<?php echo home_url('/'); ?>practice-areas/personal-injury-negligence">
				<div class="homepage-service-circle"><div id="homepage-service-circle-personal"></div></div><br>
				Personal <br>Injury	
			</a>
		</div>
		<div class="homepage-service-button col-xs-4 no-pad">
			<a href="<?php echo home_url('/'); ?>practice-areas/personal-injury-negligence">
				<div class="homepage-service-circle"><div id="homepage-service-circle-auto"></div></div><br>
				Automobile <br>Accident
			</a>
		</div>
		<div class="homepage-service-button col-xs-4 no-pad">
			<a href="<?php echo home_url('/'); ?>practice-areas/personal-injury-negligence">
				<div class="homepage-service-circle"><div id="homepage-service-circle-negligence"></div></div><br>	
				Negligence
			</a>	
		</div>
	</div>
</div>

<div class="container sub-main-width no-pad homepage-team">
	<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/homepage-team.jpg" alt="Team"/>
	<div class="three-reasons-container">
		<div class="three-reasons-banner">3 Reasons to Call Oot &amp; Associates</div>
		<div class="container sub-main-width three-reasons no-pad">
			<div class="row">
				<div class="col-xs-4 reason">
					<a href="<?php echo home_url('/'); ?>contact">Personal<br>Attention ></a>
				</div>
				<div class="col-xs-4 reason">
					<a href="<?php echo home_url('/'); ?>contact">Experienced<br>Attorneys ></a>
				</div>
				<div class="col-xs-4 reason">
					<a href="<?php echo home_url('/'); ?>contact">Thousands of<br>Satisfied Clients ></a>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container sub-main-width if-you p-container">
	<div class="row">
		<div class="col-xs-12 no-pad">
			<span>If you or a loved one has been seriously injured, it is important to seek legal representation as soon as possible. Contact the New York attorneys at Oot & Associates, PLLC for a <u>FREE</u> consultation toll-free at <a href="tel:800.435.8457">800.435.8457</a>, or in the immediate area at <a href="tel:315.471.6687" style="font-weight:normal;color:black;">315.471.6687</a>, and in the north country at <a href="tel:315.379.1466" style="font-weight:normal;color:black;">315.379.1466</a>.</span>

		</div>
	</div>
</div>


<div class="container sub-main-width awards-honors">
	<div class="row">
		<div class="col-xs-12">
			<h2>Awards &amp; Honors</h2>
		</div>
	</div>
	<div class="row">
		<ul class="award-list">
			<li><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/bvdistinguished.png" alt="BVG Distinguished"/>
</li>
			<li><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/top-lawyers-in-ny.png" alt="Top Lawyers in NY"/>
</li>
			<li><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/iwba.png" alt="IWBA"/>
</li>
		</ul>
		
		<p>The legal team at Oot & Associates, PLLC focuses on Workers’ Compensation, Social Security Disability Claims and Personal Injury Cases including automobile accidents. We represent individuals throughout Central New York, and Northern New York. Our two offices are conveniently located in Syracuse and Canton, New York. We make it easy for you to work with our experienced team of attorneys with a <u>FREE</u> Consultation. If you need help don’t go it alone or go with just any lawyer. Go with a team that has successfully handled thousands of cases just like yours. Go with Oot & Associates. Clients are accepted for representation in the New York State areas of Rochester, Geneva, Binghamton, Elmira, Cortland, Syracuse, Fulton, Oswego, Canton, Massena, Potsdam, Watertown, Gouverneur, Utica and their immediate vicinities.</p>
	</div>
</div>

<?php get_template_part('includes/footer'); ?>
