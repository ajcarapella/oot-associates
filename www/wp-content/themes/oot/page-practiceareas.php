<?php /*
Template Name: Practice Areas Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

		<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
							
							<ol class="carousel-indicators">
							<?php $i = 0; ?>
								<?php while( have_rows('home_slider') ): the_row(); 
									$slideImage = get_sub_field('slide_image');
								?>
							      	<li data-target="#homeCarousel" data-slide-to="<?php echo $i++;?>" contenteditable="false"></li>
							    <?php endwhile; ?>
						    </ol>
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">
<h1><?php the_title();?></h1>
  <div class="row practice-start">
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad content-p">
        <?php get_template_part('includes/loops/content', 'page'); ?>
	  </div> 	  
  </div><!-- /.row -->


<?php if (is_page( 'workers-compensation' ) ):?>
 	<div class="row video-list">
	 	 <!-- Modal Link 1 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#workersCompModal" data-toggle="modal">
	 			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-Attorney.jpg" alt="Why you need an attorney"/>
 			</a>
 		</div>
 		<!-- Modal Link 2 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#voluntaryRemovalModal" data-toggle="modal">
 				<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-LaborMarket.jpg" alt="Voluntary removal from the workplace"/>
 			</a>
 		</div>
 		<!-- Modal Link 3 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#compClaimModal" data-toggle="modal">
 				<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-StepsWorkersComp.jpg" alt="How to start a workers' compensation claim"/>
 			</a>
 		</div>
 		
 		
 	</div>
 	
 	
 	<div class="row video-list video-list-no-pad">
	 	
	 	
	 	<!-- Modal Link 4 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#whatCompModal" data-toggle="modal">
 				<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-WhatIsComp.jpg" alt="What is Workers' Compensation"/>
 			</a>
 		</div>
 		
 		<!-- Modal Link 5 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#howCompModal" data-toggle="modal">
 				<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-WCompClaim.jpg" alt="How to file a workers' compensation claim"/>
 			</a>
 		</div>
 		
 	</div>

 	
 	
 	<div class="row">
	 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad">
		 	<div class="panel-group" id="accordion">
			  
			  
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
			        What Injuries Qualify for Workers’ Compensation in New York?</a>
			      </h4>
			    </div>
			    <div id="collapse1" class="panel-collapse collapse">
				      <div class="panel-body">
					  		If you have been injured while at work, it is vital to understand what is and is not covered by New York’s workers’ compensation laws. These rules create strict guidelines concerning workers and their rights to pursue compensation. Just because you were at work when you were injured does not mean that you automatically qualify for benefits.
						<br><br>
						To be covered by New York compensation laws, your injury-causing incident must fall into the following categories:<br><br>
							<ul>
								<li>You must work for a company and in a capacity that is mandated to have workers’ compensation coverage under New York law.</li><br>
								<li>The injury, illness, or disability that you suffered occurred because of your job and while you were performing the duties involved in your employment.</li><br>
								<li>Your employer has notice of the incident that caused your injury or illness within 30 days of when it happened.</li><br>
								<li>A medical report states that your injury, disability, or illness was caused by an on-the-job accident or condition.</li><br>
							</ul>
						</div>
				    </div>
				  </div>
			  
			  
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
				        What are Workers’ Compensation Benefits in New York?</a>
				      </h4>
				    </div>
				    <div id="collapse2" class="panel-collapse collapse">
				      <div class="panel-body">
					      New York State law provides many different types of benefits for injured workers, with the goals of preventing financial hardship and helping victims return to work as soon as possible.
	<br><br>
	If you are found to qualify for workers' compensation, you can receive benefits including:<br><br>
							<ul>
								<li>Payments equal to 2/3 of your average weekly wage for total disability</li><br>
								<li>Reduced earning compensation to make up for a decreased ability to work if you have suffered a partial disability</li><br>
								<li>Medical treatment from doctors and chiropractors that is paid by the workers' compensation provider</li><br>
								<li>Compensation for extremity injuries, such as damage to arms, legs, hands, and feet or a facial scar, even when the damage does not keep you from working</li><br>
								<li>Reimbursement for traveling to doctors or therapists and out-of-pocket expenses related to your injury, such as bandages and prescriptions</li><br>
							</ul>
				      </div>
				    </div>
				  </div>
			  
			  
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
				        How Can A New York Workers’ Comp Attorney Help You With Your Claim?</a>
				      </h4>
				    </div>
				    <div id="collapse3" class="panel-collapse collapse">
				      <div class="panel-body">
					      These benefits are based on the nature of your injuries, the amount of time you will be unable to work, and what is needed for recovery. As such, it is vital that you collect evidence and testimony that proves the true extent of your losses so that you can gain fair and full compensation. Injured workers will have to be regularly evaluated by doctors and complete hearings with the Workers' Compensation Board to prove that their injuries or disabilities are still present and preventing work.
	<br><br>
	Without workers' compensation benefits, the cost of recovery and lost wages from being unable to work can fall squarely on your shoulders with no assistance from insurance providers. A dedicated NY workers' compensation attorney can help you secure the settlement you need.<br>
				      </div>
				    </div>
				  </div>
			  
			  
			  
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
				        Does a Workers’ Comp Case Affect Job Security?</a>
				      </h4>
				    </div>
				    <div id="collapse4" class="panel-collapse collapse">
				      <div class="panel-body">
					      Many workers who have been injured at work may feel that they can't bring a workers' compensation claim out of fear of losing their job. New York workers often assume that pursuing a workers' compensation claim will cost their employer money. This is not true. The vast majority of New York businesses carry workers' compensation insurance so they don't have to pay for worker injuries out of the company's pocket. Your workers' compensation claim should have no bearing on your job security.<br>
				      </div>
				    </div>
				  </div>
			  
			</div>
	 	</div>
 	</div>
 	
 	
    
    <!-- Modal 1 -->
    <div id="workersCompModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="workersCompVideo" width="800" height="450" src="//www.youtube.com/embed/sJKbHMVLvtM?autoplay=1" frameborder="0" allowfullscreen></iframe><p>Hello my name is Dave Pilippone. I'm a partner with Oot and Associates, I’ve been with the firm for over two decades. We handle workers’ compensation matters, social security disability claims, and personal injury matters. I've handled over 10,000 workers’ compensation hearings and our firm has handled over 40,000 workers’ compensation hearings. The first question I often receive is “Do I really need a lawyer in my workers’ compensation claim?” The simple answer to that question is absolutely yes. Whether you retain our firm or another firm it is important to have an individual with you at workers’ compensation hearings that is truly skilled in the area of the workers’ compensation law. The insurance company certainly will send a skilled representative to work against you in your claim so it's important that you obtain representation. The next question I receive is “How do workers compensation attorneys receive payment?” It’s important to understand that unlike other attorney-client relationships, a workers’ compensation attorney cannot collect money from you directly. That attorney has to submit a few requests to the workers’ compensation law judge and the judge determines whether or not that fee request is reasonable. If the judge determines the request is reasonable the fee will be awarded on the benefits the insurance company has yet to pay. Again, the payment for attorney fees comes out of money that is moving and there is no situation where an individual reaches into their own pocket and sends a check to an attorney for workers’ compensation representation. Given those facts I think it is very important to retain counsel and also to understand that you really aren't at risk of running into a situation where you run up a legal bill that you can't pay. The only way the lawyer gets paid as if they produce a result for you and the fee comes out of that result in an amount that the judge sets as being reasonable and fair. Based on the information I just shared with you, I think you'll agree that it is important to obtain representation for workers’ compensation matters and I would encourage you to contact our firm for assistance, thank you.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 2 -->
     <div id="voluntaryRemovalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="voluntaryRemovalVideo" width="800" height="450" src="//www.youtube.com/embed/jBKl1Z_USeI?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p>I'd like to talk to you a little bit about defense in the workers’ compensation form used by insurance carriers called Voluntary Removal from the Labor Market. In recent years, our firm has been retained more often by individuals who are receiving a partial disability rated workers’ compensation, but went to a hearing and the judge suspended their benefits. This defense of voluntary removal from the labor market is something that's generally easy to overcome if you follow a few simple steps that are established in case law. Those steps include registering with Access VR, which is the vocational entity of New York State, registering with a New York State one-stop Career Center such as CNY Works or Jobs Plus or Labor Ready and performing an independent job search log that is within the restrictions imposed by your treating physician. If you have a situation where the carrier has filed a request to suspend your benefits because they don't believe you're providing documentation of labor market attachment, I encourage you to contact our office because that is certainly something we can assist you with.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 3 -->
    <div id="compClaimModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="compClaimVideo" width="800" height="450" src="//www.youtube.com/embed/LdAS2El53nE?autoplay=1" frameborder="0" allowfullscreen></iframe><p>Hi my name is Jim McGevna, I’m a partner here with Oot and Associates and I just want to speak for a couple minutes about the initial start of your claim. At this point hopefully you’ve filed the C3, which protects your right to prosecute your claim. However, it does not mean that any action will be taken or that ultimately your claim will be established. To get the ball rolling initially it's the responsibility of the injured worker to provide what is called Prima Facie medical evidence. The board will not proceed any further until such a statement exist. It’s incumbent upon the worker or your attorney to obtain and provide such a statement to the board. This is just a fancy statement for a threshold opinion by a qualified medical professional that your condition is related to work and is disabling in some fashion. After finding a Prima Facie medical evidence is deemed to exist by the workers’ compensation board, the burden then shifts to the insurance carrier. The insurance carrier is entitled certain procedural rights and the claim will continue as they prosecute or defend their claim. The first thing that most insurance carriers do after such a finding, or even before, is to retain qualified counsel attorneys to assist them in prosecuting the claim. At this point we would suggest that you, as an injured worker, do exactly the same thing and get qualified legal counsel to support your claim and hopefully that will be Oot and Associates.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 4 -->
    <div id="whatCompModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="whatCompVideo" width="800" height="450" src="//www.youtube.com/embed/Cd23KkTyciI?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p><br>What is workers’ compensation? Workers’ Compensation is a collection of laws enacted in the state of New York that are designed to protect people hurt in the workplace. If you file a Workers’ Compensation claim you’re entitled to lost wages and medical coverage consistent with the benefits afforded you in the statute. Understand that the insurance company for your employer will have a skilled individual whose job is to make sure you don’t receive all those benefits, that's how those people measure their success in the workplace. It’s important if you do file a claim that you're well aware that you have the right to be represented by an attorney or licensed representative so that you can advocate on your behalf to receive all those benefits. Please contact our office if you’d like a no-cost plan review we'd be happy to speak with you.</p>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal 5 -->
    <div id="howCompModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="howCompVideo" width="800" height="450" src="//www.youtube.com/embed/clxVBWGlTEE?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p><br>Employers, injured workers, and medical providers all have obligations and responsibilities at the outset of workers compensation claim. Many times at the outset of a claim that person will appear at my office and indicate that they filed a claim or of course they know its compensation. Unfortunately, they are not responsible for whether you receive benefits or worse do not receive benefits. Ultimately you as the injured worker are required to provide written notice to the Workers’ Compensation Board within two years of the date of injury, as well as provide notice to your employer within a statutory time period. There are many more details as to what actually provides statutory notice and filing a claim within the statutory time period. To assist you with this you should consult with a qualified attorney like Oot & Associates as soon as possible.</p>
                </div>
            </div>
        </div>
    </div>
    
    
  <?php endif;?>
  
  
  
  <?php if (is_page( 'social-security-disability' ) ):?>
 	<div class="row video-list">
	 	 <!-- Modal Link 1 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#socialSecurityModal" data-toggle="modal">
	 			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-SocialSecurity.jpg" alt="Social Security Disability"/>
 			</a>
 		</div>
 		
 		 <!-- Modal Link 2 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#howSocialModal" data-toggle="modal">
	 			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-SSDClaim.jpg" alt="How to file a Social Security claim"/>
 			</a>
 		</div>
 	</div>
 	
    
    <!-- Modal 1 -->
    <div id="socialSecurityModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="socialSecurityVideo" width="800" height="450" src="//www.youtube.com/embed/eV8x4oP5DHc?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p>Hello I'm Neal McCurn Jr. I'm a partner with Oot and Associates law firm where I’ve been practicing social security law and personal injury law for well over 20 years. Have you been disabled from work for 12 months or likely to be disabled from work for 12 months? If so, you might want to consider filing a claim for Social Security disability benefits. Social Security disability benefits are available if you meet the definition of disability under the statute. That depends on the particular age, education and former work experience that you may have. For instance, if you are under the age of 50 we would need to establish that you are not only precluded from your past relevant work, but moreover from all work completely. If you are between the ages of 50 and 55 we need to establish that you cannot perform your past relevant work and also that at best you can perform sedentary or the lightest level of work that exists. If you're over the age of 55 we need to establish that you are incapable of performing your past relevant work. Social Security considers your past relevant work any jobs that you have done over the 15 years prior to the onset of your disability date. Social security claims are very complex there’s a lot of time constraints, a lot of paperwork and some very complex issues. If you believe you may qualify for Social Security disability benefits I strongly encourage you to seek representation. I would be more than happy to sit down with you to discuss your claim at your convenience. Please feel free to call me at any time. Understand if we do take your claim there is no fee for our services unless we are successful in securing benefits for you, thank you.</p>
                </div>
            </div>
        </div>
    </div>
    
        <!-- Modal 2 -->
    <div id="howSocialModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="howSocialVideo" width="800" height="450" src="//www.youtube.com/embed/nxuIDFxy-CA?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p><br>If you've been injured and are out of work and expect to be out of work for 12 consecutive months, you should consider filing for Social Security Disability. Social Security Disability is available for individuals who have sufficient quarters of coverage based on the work history. Other individuals who do not have sufficient coverage can apply for SSI, the medical criteria are the same or similar, but we can make that determination based upon our review of your case. In order to qualify for Social Security Disability, as I said, you need to be out or expect to be out for 12 consecutive months and then you also have to have a significant disability. The degree of disability depends on your age category. Social Security puts you into three different categories primarily. There's a younger individual, which are people under 50. People approaching advanced age which is 50-55. And advanced age where you're over 55. If you're under 50 we have to prove that you're not able to perform your past relevant work. Social security will look at the 15 years prior to your onset of disability date and they'll consider every job that you've done over that period of time. If you have the ability to perform any of those jobs you do not qualify. Furthermore, if you're under 50 we have to prove that you're not able to perform any other work that exists in the national economy. So it's a significant burden, however, we have many many clients in that age category and we've had great success in getting benefits for them. When you attain the age of 50 they put you in advanced age category. In that instance we need to establish 1, that you're not able to do your past relevant work and 2, that at best you can perform what they deem sedentary level work and that's the lightest level of work that exists. If you meet those two criteria then you should qualify. Lastly if you're over the age of 55 Social Security is going to look at your past relevant work only and if you can’t perform your past relevant work you typically qualify. There are other criteria that come into play, which we won't get into here, that's more complex talks about transferable work skills etc. In every case is different in that regard so I won't spend time talking about that now. If you feel that you may qualify for Social Security disability under the criteria that I have identified, please give me a call and we'll sit down and talk about it and if appropriate file a claim. Thank you.</p>
                </div>
            </div>
        </div>
    </div>
  <?php endif;?>
  
  
  
  
  <?php if (is_page( 'personal-injury-negligence' ) ):?>
 	<div class="row video-list">
	 	 <!-- Modal Link 1 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#injuredClaimModal" data-toggle="modal">
	 			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-Experience.jpg" alt="Experience With Personal Claim Law"/>
 			</a>
 		</div>
 		<!-- Modal Link 2 -->
 		<div class="col-xs-12 col-sm-3">
 			<a href="#personalInjuryModal" data-toggle="modal">
 				<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/Video-PersonalInjury.jpg" alt="Understanding Personal Injury"/>
 			</a>
 		</div>
 	</div>
 	
    
    <!-- Modal 1 -->
    <div id="injuredClaimModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="injuredClaimVideo" width="800" height="450" src="//www.youtube.com/embed/fHTVCLo45hM?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p>Hello I'm Leah Oot, I'm a partner here at Oot and Associates. I've been with the law firm for over two decades, but really I've been a part of Oot and Associates since birth. My father, Thaddeus Oot, founded the law firm over half a century ago. I'm very proud to carry on my father's legacy.
He was one of the leading and foremost authorities in central New York in campaigning the rights of injured workers. Like my dad, I have a thorough understanding of the law. In addition, prior to becoming an attorney, I was a registered nurse with experience in orthopedic surgery, intensive care and trauma O.R. I have a strong medical background that I think is vital in understanding the needs and the issues of workers’ compensation matters. Having a comprehensive understanding of both law and medicine makes me uniquely qualified to help injured workers. If you've been injured on the job do yourself a favor and call Oot and Associates.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 2 -->
     <div id="personalInjuryModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <iframe id="personalInjuryVideo" width="800" height="450" src="//www.youtube.com/embed/ic7M17R_ugo?autoplay=1" frameborder="0" allowfullscreen></iframe>
                    <p>Hello I'm Neal McCurn Jr. I’m a partner with Oot and Associates law firm. For the past 20-plus years I've been concentrating my legal practice in the areas of personal injury and social security disability. If you’ve been injured in an auto accident, a slip-and-fall or other
accident, it's important that you retain counsel right away. The insurance carriers are going to be well represented by attorneys. If you are not represented you're at a serious disadvantage. Understand that there are certain time limits with every personal injury claim and should you miss one of those time constraints your claim will be seriously jeopardized or possibly precluded entirely. Seek counsel, retain advice and make sure you don't miss those deadlines. Have you been injured? If so, please contact me at Oot and Associates. I'd be happy to meet with you free of charge to discuss your claim. If we do take your claim there will be no fee for legal services unless we are successful in securing an award for you. Thank you for your time.</p>
                </div>
            </div>
        </div>
    </div>
  <?php endif;?>

  
  
  <div class="row practice-start-box">
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad content-p">
		  <h3 class="practice-contact">Contact Us: FREE Consultation</h3>
        <p class="practice-p">If you or a loved one has been seriously injured, it is important to seek legal representation as soon as possible. Contact the New York attorneys at Oot & Associates, PLLC for a <u>FREE</u> consultation toll-free at <a href="tel:800.435.8457">800.435.8457</a>, or in the immediate area at 315.471.6687, and in the north country at 315.379.1466.</p>
	  </div> 
	  
	  
	    <br>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad back-to-prac">
  	<a href="<?php echo home_url('/'); ?>practice-areas">&laquo; Back to Practice Areas</a>
  </div>
	  
  </div><!-- /.row -->
  

  
    
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>