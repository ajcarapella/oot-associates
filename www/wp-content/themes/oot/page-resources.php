<?php /*
Template Name: Resources Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

			<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
							
							
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">

<h1>Resources</h1>
  <div class="row team-start">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 test-box no-pad">
	  <?php get_template_part('includes/loops/content', 'page'); ?>
	  <?php while( have_rows('resources') ): the_row(); 
								$resourceTitle = get_sub_field('title');
								$resourceLink = get_sub_field('link');
								$resourceDescription = get_sub_field('description');
								?>
								<div class="row resource-box">
						                <h2><?php echo $resourceTitle; ?></h2>
						                <h3><a href="<?php echo $resourceLink; ?>" target="_blank"><?php echo $resourceLink; ?></a></h3>
										<p><?php echo $resourceDescription; ?></p>
								</div>
		<?php endwhile; ?>

	  
	  
	  </div> 
	  	  
  </div><!-- /.row -->
    
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>