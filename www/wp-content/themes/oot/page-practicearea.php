<?php /*
Template Name: Practice Area Main Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

		<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
							
							<ol class="carousel-indicators">
							<?php $i = 0; ?>
								<?php while( have_rows('home_slider') ): the_row(); 
									$slideImage = get_sub_field('slide_image');
								?>
							      	<li data-target="#homeCarousel" data-slide-to="<?php echo $i++;?>" contenteditable="false"></li>
							    <?php endwhile; ?>
						    </ol>
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">
<h1><?php the_title();?></h1>
  <div class="row practice-start">
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad content-p">
        <?php get_template_part('includes/loops/content', 'page'); ?>
	  </div> 	 
	  
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad practice-area">
	  	<div class="row no-pad">
	  		<div class="col-xs-4 no-pad">
	  			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/1411_workerscomp.jpg"/>
	  		</div>
	  		<div class="col-xs-8">
	  			<h2>New York Workers' Compensation</h2>
	  			<h3>Filing a Workers' Compensation Claim in New York</h3>
	  			<p>A work-related injury is serious business. It can affect your performance or your ability to perform your job now or in the future.</p>
	  			<a href="<?php echo home_url('/'); ?>practice-areas/workers-compensation"><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/practice-readmore.png"/></a>
	  		</div>
	  	</div>
	  </div>
	  
	  
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad practice-area">
	  	<div class="row no-pad">
	  		<div class="col-xs-4 no-pad">
	  			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/1412_ssd.jpg"/>
	  		</div>
	  		<div class="col-xs-8">
	  			<h2>Social Security Disability</h2>
	  			<h3>Get the Social Security Disability Benefits You Deserve</h3>
	  			<p>If you have paid into the Social Security system, you are entitled to disability benefits if you are unable to work.</p>
	  			<a href="<?php echo home_url('/'); ?>practice-areas/social-security-disability"><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/practice-readmore.png"/></a>
	  		</div>
	  	</div>
	  </div>
	  
	  
	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad practice-area">
	  	<div class="row no-pad">
	  		<div class="col-xs-4 no-pad">
	  			<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/stairs.jpg"/>
	  		</div>
	  		<div class="col-xs-8">
	  			<h2>Personal Injury/Negligence</h2>
	  			<h3>We are Personal Injury Lawyers Who Fight For Your Rights</h3>
	  			<p>Were you injured by the negligence or recklessness of others? If so, you owe it to yourself to call us for a FREE consultation today!</p>
	  			<a href="<?php echo home_url('/'); ?>practice-areas/personal-injury-negligence"><img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/practice-readmore.png"/></a>
	  		</div>
	  	</div>
	  </div>
	  
	   
  </div><!-- /.row -->

   
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>