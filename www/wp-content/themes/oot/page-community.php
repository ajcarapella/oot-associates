<?php /*
Template Name: Community Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

			<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
					
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">
<h1>Community</h1>


  <div class="row community-start">
	  
	  
	  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad">
        <?php get_template_part('includes/loops/content', 'page'); ?>
	  
	  <?php while( have_rows('community') ): the_row(); 
								$communityHeadline = get_sub_field('title');
								$communityDescription = get_sub_field('description');
								$communityLink = get_sub_field('link');
								?>
								
								 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 community-box no-pad <?php if( get_sub_field('description') == false ): ?>no-description<?php endif; ?>">
						                <h2><?php echo $communityHeadline; ?></h2>
						                <?php if( get_sub_field('description') ): ?>
						                	<p><?php echo $communityDescription; ?></p>
										<?php endif; ?>
										<?php if( get_sub_field('link') ): ?>
						              	  <a href="http://<?php echo $communityLink; ?>" target="_blank"><?php echo $communityLink; ?></a>
										<?php endif; ?>
						        </div>
		<?php endwhile; ?>

	  
	  
	  </div> 
	  	  
  </div><!-- /.row -->
  
  
  
<p class="disclaimer" style="font-size:13px;">Oot & Associates, PLLC is not a direct affiliate, does not endorse and may not be a routine sponsor of any organization listed above.
</p>
  
    
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>