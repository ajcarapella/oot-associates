<?php /*
Template Name: Legal Team Page
*/ 
get_template_part('includes/header'); ?>

<?php if( have_rows('home_slider') ): ?>

			<div class="container main-width no-pad pr">
					<div id="homeCarousel" class="carousel slide carousel-fade">
						<div class="carousel-inner">
							<?php while( have_rows('home_slider') ): the_row(); 
								$slideImage = get_sub_field('slide_image');
								$slideTitle = get_sub_field('slide_caption');
								$slideSubTitle = get_sub_field('slide_sub_caption');
								?>
								
								 <div class="item">
						            <img src="<?php echo $slideImage; ?>" alt="<?php echo $slideTitle; ?>"/>
						            <div class="carousel-caption">
						                <h2><?php echo $slideTitle; ?></h2>
						                <h3><?php echo $slideSubTitle; ?></h3>
						            </div>
						        </div>
							<?php endwhile; ?>
							
							
							<ol class="carousel-indicators">
							<?php $i = 0; ?>
								<?php while( have_rows('home_slider') ): the_row(); 
									$slideImage = get_sub_field('slide_image');
								?>
							      	<li data-target="#homeCarousel" data-slide-to="<?php echo $i++;?>" contenteditable="false"></li>
							    <?php endwhile; ?>
						    </ol>
							
						</div>
					</div>
					
					<div class="container consultation-form fixed-consult-form">
						<div id="title-bar">
							Schedule Your <br>FREE Consultation
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="21" title="Schedule Your Free Consultation"]' );?>
						<div class="close-consult">X</div>
					</div>
					
					<div class="consult-activate">
						<img src="<?php echo home_url('/'); ?>wp-content/themes/oot/images/tab-free-consultation.png" alt="Click for Free Consultation"/>
					</div>
					
				</div>
			
				<script>jQuery( '#homeCarousel .carousel-inner').find('.item:first' ).addClass( 'active' );
					jQuery( '#homeCarousel .carousel-inner .carousel-indicators').find('li:first' ).addClass( 'active' );
				</script>

<?php endif; ?>


<div class="container sub-main-width main-container">
<h1>Our Legal Team at Oot & Associates</h1>
  <div class="row team-start">
	  
	
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad">  
	    <style>
		p {font-size:21px;line-height:26px;}</style>
		<?php get_template_part('includes/loops/content', 'page'); ?>
        </div>
	  
	  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-pad">
        
     	
      
	  <?php while( have_rows('team') ): the_row(); 
								$teamImage = get_sub_field('image');
								$teamName = get_sub_field('name');
								$teamTitle = get_sub_field('title');
								$teamEmail = get_sub_field('email');
								$teamPhone = get_sub_field('phone');
								$teamDescription = get_sub_field('description');
								$excerpt = wp_trim_words( get_sub_field('description' ), $num_words = 47, $more = '' );
								?>
								<div class="row team-box">
									<div class="col-xs-4 col-sm-4 no-pad">
										<img src="<?php echo $teamImage; ?>" alt="<?php echo $teamName; ?>"/>
									</div>
									<div class="col-xs-8 col-sm-8 no-pad-right-mobile">
						                <h2 class="teamName"><?php echo $teamName; ?></h2>
						                <h3 class="teamTitle"><?php echo $teamTitle; ?></h3>
						                <h3 class="teamEmail"><a href="mailto:<?php echo $teamEmail; ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $teamEmail; ?></a></h3>
						                <h3 class="teamTelephone"><a class="mobile-number-larger" href="tel:<?php echo $teamPhone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $teamPhone; ?></a></h3>
						                <?php if( get_sub_field('description') ): ?>
						                	<p class="hidden-xs"><?php echo $excerpt; ?>...
						                		<a href="#<?php echo str_replace(' ','',get_sub_field('name'));?>Modal" data-toggle="modal" class="hidden-xs">Read More &raquo;</a></p>
						                		<a href="#<?php echo str_replace(' ','',get_sub_field('name'));?>Modal" data-toggle="modal" class="hidden-sm hidden-md hidden-lg">Read Attorney Bio &raquo;</a>

										<?php endif; ?>
									</div>
								</div>
								
								 <!-- Modal -->
							    <div id="<?php echo str_replace(' ','',get_sub_field('name'));?>Modal" class="modal fade" style="top:0;">
							        <div class="modal-dialog">
							            <div class="modal-content">
							                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							                <div class="modal-body">
								                <div class="row">
									                <div class="col-xs-12 col-sm-4">
									                	<img style="width:100%;" src="<?php echo $teamImage; ?>" alt="<?php echo $teamName; ?>"/>
									                </div>
									                <div class="col-xs-12 col-sm-8">
										                <h2 class="teamNameMobile"><?php echo $teamName; ?></h2>
										                <h3 class="teamTitleMobile"><?php echo $teamTitle; ?></h3>
										                <h3 class="teamEmailMobile"><a href="mailto:<?php echo $teamEmail; ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $teamEmail; ?></a></h3>
										                <h3 class="teamTelephoneMobile"><a href="tel:<?php echo $teamPhone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $teamPhone; ?></a></h3>
										                <?php if( get_sub_field('description') ): ?>
										                	<p style="font-size:16px!important;line-height:22px!important;color:rgb(54, 24, 13);padding-top:5px;"><?php echo $teamDescription; ?></p>
														<?php endif; ?>
									                </div>
								                </div>

							                 </div>
							            </div>
							        </div>
							    </div>

		<?php endwhile; ?>

	  </div> 
	  	  
  </div><!-- /.row -->
    
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>